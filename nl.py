from datetime import date, timedelta
from matplotlib import pyplot as plt
from io import StringIO

import requests
import pandas as pd

FORMAT_URL = r"https://www.volksgezondheidenzorg.info/sites/default/files/map/detail_data/klik_corona{}.csv"
# some of them are corrected, and when downloading we should first check if
# there is one
FORMAT_CORRECTED_URL = r"https://www.volksgezondheidenzorg.info/sites/default/files/map/detail_data/klik_corona{}_rectificatie.csv"
DATE_FORMAT = "%d%m%Y"

FIRST_DAY = date(2020, 3, 3) # don't think there are earlier urls
LAST_DAY = date(2020, 3, 8)

N_DAYS = (LAST_DAY - FIRST_DAY).days + 1

total_counts = []
days = [FIRST_DAY + timedelta(days=i) for i in range(N_DAYS)]

def get_day_df(day):
    """Returns a dataframe corresponding to the given date, if it exists
    online.
    """
    # first try if there is a corrected version (like on 2020-03-08)
    url = FORMAT_CORRECTED_URL.format(day.strftime(DATE_FORMAT))
    response = requests.get(url)
    if response.status_code != 200:
        url = FORMAT_URL.format(day.strftime(DATE_FORMAT))
        response = requests.get(url)
        if response.status_code != 200:
            raise ValueError("No data available for given date: {}".format(day.isoformat()))
    df = pd.read_csv(StringIO(response.text), delimiter=";")
    df = df.fillna(0) # clear our NaN from last column
    return df

for day in days:
    df = get_day_df(day)
    total_counts.append(df['Aantal'].sum())
    print("  Most infected counties")
    for idx, row in df.sort_values("Aantal", ascending=False).head(10).iterrows():
        print("    {}: {}".format(row.Gemeente, row.Aantal))

plt.plot(days, total_counts)
plt.show()
